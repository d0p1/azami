#!/usr/bin/perl -s
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <dopi-sama@hush.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return. d0p1 
# ----------------------------------------------------------------------------

package AzamiBot;

use strict;
use warnings;
use utf8;

use POSIX;
use File::Pid;
use Cwd;

use File::Spec::Functions 'catfile';

use YAML::Syck;

use base qw(Bot::BasicBot);
use vars qw( $path );

my $config = {
	host => "localhost",
	port => "6697",
	ssl => 1,
	password => undef,
	nick => "Azami",
	username => "azami",
	realname => "あざみIRC bot",
	nickserv => undef,
	channels => [],
	commandprefix => '!'
};
utf8::encode($config->{'realname'});

my $users_list = {

};

my $azami_path = (!defined($path) ? getcwd : $path);
my $config_file = catfile($azami_path, "azami.conf");
my $pid_file = catfile($azami_path, "azami.pid");

# fnc

sub save_to_config_file {
	open (my $fh, '>', $config_file) or die "Can't open $config_file";
	print $fh YAML::Syck::Dump($_[0]);
	close $fh;
}

sub get_data_or_default {
	return ((defined($_[0]) && !($_[0] eq '')) ? $_[0] : $_[1]);
}

sub signal_handler {
	exit 0;
}

sub connected {
	my $self = shift;
	if (defined($config->{'nickserv'}) && $config->{'nickserv'} ne '') {
		$self->say(channel => "msg", who => "nickserv", body => "identify $config->{'nickserv'}");
	}
	return undef;
}

sub chanjoin {
	my ($self, $msg) = @_;

	if ($msg->{'who'} ne $config->{'nick'}) {
		return "Hello $msg->{'who'}";
	}
	return undef;
}

sub process_cmd {
	my ($self, $channel, $who, $cmd, @args) = @_;

	if ($cmd eq 'beer') {
		if (defined($args[0]) && $args[0] ne '') {
			$self->emote(channel => $channel, body => "offre une bière à $args[0] de la part de $who");
		} else {
			$self->emote(channel => $channel, body => "offre une bière à $who");
		}
	} elsif ($cmd eq 'buddha') {
		$self->say(channel => $channel, body => "     _=_");
		$self->say(channel => $channel, body => "   q(-_-)p");
		$self->say(channel => $channel, body => "   '_) (_`");
		$self->say(channel => $channel, body => "   /__/  \\");
		$self->say(channel => $channel, body => " _(<_   / )");
		$self->say(channel => $channel, body => "(__\\_\\_|_/__)");
	} elsif ($cmd eq 'coffee') {
		if (defined($args[0]) && $args[0] ne '') {
			$self->emote(channel => $channel, body => "offre un café à $args[0] de la part de $who");
		} else {
			$self->emote(channel => $channel, body => "offre un café à $who");
		}
	}
}

sub said {
	my ($self, $msg) = @_;

	if (!defined($users_list->{$msg->{'who'}})) {
		my $text = $msg->{'body'};
		if ($text =~ s/^(\Q$config->{'commandprefix'}\E)\s*[:,-]?\s*//i) {
			$text =~ s/!config->{'commandprefix'}\s+//;
			my @args = split(' ', $text);
			my $command = shift @args;
			if ($command ne '') {
				process_cmd($self, $msg->{'channel'}, $msg->{'who'}, $command, @args);
				$users_list->{$msg->{'who'}} = 1;
			}
		}
	}
	return undef;
}

sub tick {
	for (keys %$users_list)
    {
        delete $users_list->{$_};
    }
	return 15;
}


# main
if ( -f $pid_file) {
	die "Another instance is already running :'(";
}
unless ( -f  $config_file) {
	my $server = undef;
	while (!(defined($server)) || $server eq '')
	{
		print "[+] Host: ";
		chomp($server = <STDIN>);
	}
	print "[+] Port (default: $config->{'port'}): ";
	chomp(my $port = <STDIN>);
	my $ssl = undef;
	while (!(defined($ssl)) || ((uc($ssl) ne "TRUE") && (uc($ssl) ne "FALSE")))
	{
		print "[+] SSL (true/false): ";
		chomp($ssl = <STDIN>);
	}
	print "[+] Server password (default: None): ";
	chomp(my $password = <STDIN>);
	print "[+] Nick (default: $config->{'nick'}) :";
	chomp(my $nick = <STDIN>);
	print "[+] Username (default: $config->{'username'}) :";
	chomp(my $username = <STDIN>);
	print "[+] Realname (default: $config->{'realname'}) :";
	chomp(my $realname = <STDIN>);
	my $channel = undef;
	while (!(defined($channel)) || $channel eq '')
	{
		print "[+] Channel (multiple chan must be separated by comma): ";
		chomp($channel = <STDIN>);
	}
	$channel =~ s/^\s*(.*?)\s*$/$1/;
	my @channels = split(/,/, $channel);
	print "[+] Command Prefix (default: $config->{'commandprefix'}) :";
	chomp(my $commandprefix = <STDIN>);

	$config->{'host'} = get_data_or_default($server,  $config->{'host'});
	$config->{'port'} = get_data_or_default($port, $config->{'port'});
	$config->{'ssl'} = (uc($ssl) eq "TRUE");
	$config->{'password'} = get_data_or_default($password, $config->{'password'});
	$config->{'nick'} = get_data_or_default($nick, $config->{'nick'});
	$config->{'username'} = get_data_or_default($username, $config->{'username'});
	$config->{'realname'} = get_data_or_default($username, $config->{'realname'});
	$config->{'channels'} = \@channels;
	save_to_config_file($config);
}

# demonify
chdir '/';
umask 0;
open STDIN, "/dev/null" or die "Can't read /dev/null :<";
open STDOUT, ">>/dev/null" or die "Can't write /dev/null :<";
open STDERR, ">>/dev/null" or die "Can't write /dev/null";

my $pid = fork;
if (!defined($pid)) {
	die "Can't fork :<";
}
exit if $pid;
POSIX::setsid() or die "Can't create new sid";

my $pid_data = File::Pid->new({
	file => $pid_file
});

$pid_data->write or die "Can't write pid file";

$SIG{INT} = $SIG{TERM} = $SIG{HUP} = \&signal_handler;

# run program
$config = YAML::Syck::LoadFile($config_file);

my $ircbot = AzamiBot->new(
	nick => $config->{'nick'},
	username => $config->{'username'},
	name => $config->{'realname'},
	server => $config->{'host'},
	password => $config->{'password'},
	port => $config->{'port'},
	ssl => $config->{'ssl'},
	channels => $config->{'channels'}
);

$ircbot->run();

END {
	if (defined($ircbot)) {
		$ircbot->shutdown($ircbot->quit_message());
	}
	if (defined($pid_data)) {
		$pid_data->remove;
	}
}
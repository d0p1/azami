# Azami ~ Perl IRC Bot

## Config

* `host`: server host (eg: `irc.exemple.net`)
* `port`: server port (eg: `6667`)
* `ssl`: true  or false
* `password`: (server password)
* `nick`: (eg: Azami)
* `username`: (eg: Azami)
* `realname`: (eg: あざみIRC bot)
* `nickserv`: nickserv password
* `channels`: eg:
```yaml
channels:
    - "#chan1"
    - "#chan2"
```
* `commandprefix`: prefix before command (eg: !)

#### Azami.conf.example

```yaml
--- 
host: irc.exemple.net
port: 6697
ssl: 1
password: ~
channels: 
  - "#bots"
nick: Azami
username: azami
realname: あざみIRC bot
commandprefix: "!"
```

## License

"THE BEER-WARE LICENSE" (Revision 42):
<dopi-sama@hush.com> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. d0p1 